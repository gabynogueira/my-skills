import React, { useState, useEffect } from 'react';
import { 
  TextInput,
  Text, 
  View,
  FlatList,

 
} from 'react-native';


import { styles } from './styles';
import { Button } from '../components/Button';
import { HabilidadeCard } from '../components/HabilidadeCard';

interface DadosDeHabilidade {
  id: string;
  name: string;
  date?: Date;
}


export default function Home() {
  const [minhaHabilidade,setMinhaHabilidade] = useState('');
  const [minhasHabilidades,setMinhasHabilidades] = useState<DadosDeHabilidade[]>([]);
  const [saudacao,setSaudacao] = useState('');

  
  function handleAddMinhaHabilidade(){
    const data = {
      id:String(new Date().getTime()),
      name: minhaHabilidade
    }

    setMinhasHabilidades(oldState => [...oldState, data]);
  }

  useEffect(()=>{
    const horaAtual = new Date().getHours();
    
    if(horaAtual < 12){
      setSaudacao('Bom Dia!');
    }
    else if(horaAtual >= 12 &&  horaAtual < 18){
      setSaudacao('Boa Tarde!');
    }
    else{
      setSaudacao('Boa Noite!');
    }


  },[])


  
  return (
    <View style={styles.container}>
      
      <Text style={styles.title}>Olá, Gabriela</Text>

      <Text style={styles.saudacoes}>{saudacao} </Text>

      <TextInput
        style={styles.input}
        placeholder="Minha habilidade"
        placeholderTextColor="#555"
        onChangeText={setMinhaHabilidade}
      />

      <Button 
        title="Add"
        onPress={handleAddMinhaHabilidade}
        // activeOpacity={0.1}  
        
      />
   

      <Text style={[styles.title, {  marginVertical: 50}]}>
         Minhas habilidades
      </Text>

      

      
      <FlatList  
        data={minhasHabilidades}
        keyExtractor={item => item.id}
        renderItem={({ item }) =>(
          <HabilidadeCard habilidade={item.name}/>
        )}
      />
          
      
    </View>
  );
}


