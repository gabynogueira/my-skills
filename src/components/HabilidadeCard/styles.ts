import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    
    buttonHabilidade:{
        backgroundColor: '#1f1e25',
        padding: 15,
        borderRadius: 50,
        alignItems: 'center',
        marginVertical: 10
    },
    textHabilidade:{
        color: '#fff',
        fontSize: 22,
        fontWeight: 'bold',
    }
})