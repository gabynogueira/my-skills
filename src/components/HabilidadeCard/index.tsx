import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { styles } from './styles';

export function HabilidadeCard({habilidade}){
  return(
        <TouchableOpacity  style={styles.buttonHabilidade}>
            <Text style={styles.textHabilidade}>
                {habilidade}
            </Text>
        </TouchableOpacity>
  )
}